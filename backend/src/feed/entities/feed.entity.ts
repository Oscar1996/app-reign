import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Feed extends Document {
  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  created_at: string;

  @Prop()
  author: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;
}

export const FeedSchema = SchemaFactory.createForClass(Feed);
