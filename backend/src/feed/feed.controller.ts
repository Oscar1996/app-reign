import {
  Controller,
  HttpService,
  Get,
  Post,
  Delete,
  Param,
  Query,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { FeedService } from './feed.service';

@Controller('feed/')
export class FeedController {
  constructor(
    private readonly feedService: FeedService,
    private httpService: HttpService
  ) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  findAll(@Query() paginationQuery: PaginationQueryDto) {
    return this.feedService.findAll(paginationQuery);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  findOne(@Param('id') id: string) {
    return this.feedService.findOne(id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  createMany() {
    this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe((res) => {
        this.feedService.createMany(res.data.hits);
        return;
      });
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  remove(@Param('id') id: string) {
    return this.feedService.remove(id);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  removeAll() {
    return this.feedService.removeAll();
  }
}
