import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed } from './entities/feed.entity';
import { CreateManyDto } from './dto/create-many.dto';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';

@Injectable()
export class FeedService {
  constructor(
    @InjectModel(Feed.name) private readonly feedModel: Model<Feed>
  ) {}

  findAll(paginationQuery: PaginationQueryDto) {
    const { limit, offset } = paginationQuery;
    return this.feedModel
      .find()
      .limit(limit)
      .skip(offset)
      .sort({ created_at: -1 })
      .exec();
  }

  async findOne(id: string) {
    const feed = await this.feedModel.findOne({ _id: id }).exec();
    if (!feed) throw new NotFoundException(`Feed with id: ${id} not found`);
    return feed;
  }

  async createMany(createManyDto: CreateManyDto[]) {
    await this.feedModel.insertMany(createManyDto);
    return;
  }

  async remove(id: string) {
    const feed = await this.findOne(id);
    return feed.remove();
  }

  async removeAll() {
    await this.feedModel.deleteMany({});
  }
}
