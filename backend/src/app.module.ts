import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeedModule } from './feed/feed.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/test-reign', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    HttpModule,
    FeedModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
