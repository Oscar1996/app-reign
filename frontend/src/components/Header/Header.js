import { React } from 'react';
import './Header.css';

export const Header = () => {
  const text = 'We <3 hacker news!';

  return (
    <>
      <header className="header">
        <h1 className="title">HN Feed</h1>
        <p>{text}</p>
      </header>
    </>
  );
};
