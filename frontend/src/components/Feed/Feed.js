import { React, useState, useEffect } from 'react';
import { feedApi } from '../../api/feedApi';
import { RiDeleteBinFill } from 'react-icons/ri';

import './Feed.css';

export const Feed = () => {
  const [data, setData] = useState([]);

  const fetchFeed = () => {
    feedApi
      .get('/feed', {
        params: {
          limit: 20,
        },
      })
      .then((feedData) => {
        setData(feedData.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const deleteFeed = async (id) => {
    await feedApi.delete(`/feed/${id}`);
    fetchFeed();
  };

  useEffect(() => {
    fetchFeed();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <ul>
        {data.map(
          ({ _id, story_title, title, author, created_at, url, story_url }) => {
            return (
              <div>
                <a
                  className="box-title"
                  href={url ? url : story_url ? story_url : null}
                  target="_blank"
                  rel="noreferrer"
                  key={_id}
                >
                  <li className="box-list">
                    {title ? title : story_title ? story_title : null}.{' '}
                    <span className="box-author">- {author} -</span>{' '}
                    <span className="box-date">{created_at.slice(0, 10)}</span>
                    <button
                      className="box-button"
                      onClick={() => deleteFeed(_id)}
                    >
                      <RiDeleteBinFill size="2em" />
                    </button>
                  </li>
                </a>
              </div>
            );
          }
        )}
      </ul>
    </>
  );
};
