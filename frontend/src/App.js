import { Header } from './components/Header/Header';
import { Feed } from './components/Feed/Feed';

function App() {
  return (
    <div>
      <Header />
      <Feed />
    </div>
  );
}

export default App;
