# app-reign

App Reign

## Installation

Docker, NoseJS and Postman (or other api tester) installed.

To initialize the front-end and mongo:

### `docker-compose up --build`.

To initialize the back-end

### `cd /backend`.

### `npm run start:dev`.

To populate the database run (in postman):

### `Post request to: http://localhost:8000/feed`

To drop the database run (in postman):

### `Delete request to: http://localhost:8000/feed`
